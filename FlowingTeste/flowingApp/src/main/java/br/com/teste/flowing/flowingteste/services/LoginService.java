/**
 * 
 */
package br.com.teste.flowing.flowingteste.services;

import android.content.Context;
import android.util.Log;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;

import br.com.teste.flowing.flowingteste.R;
import br.com.teste.flowing.flowingteste.services.retrofit.IServices;
import br.com.teste.flowing.flowingteste.services.retrofit.IServicesFactory;
import br.com.teste.flowing.flowingteste.services.so.LoginSO;
import br.com.teste.flowing.flowingteste.utils.Preferencias_;
import retrofit.client.Response;
import retrofit.Callback;
import retrofit.RetrofitError;

/**
 *(scope=Scope.Singleton)
 */
@EBean
public class LoginService implements SyncListener {

	protected SyncListener listener;

	protected Context context;

	protected IServices iServices;

	@Pref
	protected Preferencias_ preferencias;

	public LoginService(Context context) {
		this.context = context;
		this.iServices = IServicesFactory.build(context);
	}


	public void login(final String userLogin, final String userPass, SyncListener listener){

		this.listener = listener;

		if (userLogin == null || userLogin.isEmpty() || userPass == null || userPass.isEmpty()){
			onSyncError("Dados de login inválidos", null);
		}

//		final String hashSenha = geraHash(senha);

		iServices.obterLogin(context.getString(R.string.grantType), userLogin, userPass, new Callback<LoginSO>() {

			@Override
			public void success(LoginSO result, Response arg1) {

				if(result == null || result.getToken() == null || result.getToken().isEmpty() ||
						result.getTokenType() == null || result.getTokenType().isEmpty()){
					onSyncError("Erro ao fazer login!", null);
				}else{
					onFinishLogin(userLogin, userPass, result);
				}

			}

			@Override
			public void failure(RetrofitError e) {
				onSyncError(e.getMessage(), e);
			}
		});
	}

	protected void onFinishLogin(String userLogin, String userPass, LoginSO result){

		LoginSO loginData = result;

		preferencias.token().put(loginData.getToken());
		preferencias.tokenType().put(loginData.getTokenType());
		onSyncFinished(result);

/*		this.login = login;
		this.hashSenha = hashSenha;*/
	}

	@UiThread
	@Override
	public void onSyncStatusUpdated(String updateMessage) {
		Log.i(getTag(), updateMessage);

		if (listener != null){
			listener.onSyncStatusUpdated(updateMessage);
		}
	}

	@UiThread
	@Override
	public void onSyncFinished(Object result) {
		Log.i(getTag(), "Serviço finalizado!");

		if (listener != null){
			listener.onSyncFinished(result);
		}
	}

	@UiThread
	@Override
	public void onSyncError(String message, Exception e) {
		Log.e(getTag(), message, e);

/*		if (e != null){
			Mint.logException(getTag(), message, e);
		}*/

		if (listener != null){
			listener.onSyncError(message, e);
		}
	}

	public String getTag(){
		return this.getClass().getSimpleName();
	}

/*	protected String geraHash(String senha){
		return Encrypt.md5(senha);
	}*/
}