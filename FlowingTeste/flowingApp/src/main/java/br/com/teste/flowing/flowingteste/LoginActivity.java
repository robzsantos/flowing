package br.com.teste.flowing.flowingteste;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.WindowFeature;

import br.com.teste.flowing.flowingteste.services.LoginService;
import br.com.teste.flowing.flowingteste.services.SyncListener;

/**
 * @author rsantos
 */
@WindowFeature({Window.FEATURE_NO_TITLE})
@EActivity(R.layout.activity_login)
public class LoginActivity extends Activity {

    protected static final String TAG = LoginActivity.class.getName();

    @ViewById
    protected EditText edTxtUsername;

    @ViewById
    protected EditText edTxtPassword;

    @Bean
    protected LoginService loginService;

    private ProgressDialog pd;

    @Override
    protected void onResume() {
        super.onResume();

        edTxtUsername.setText("");
        edTxtUsername.setEnabled(true);

        edTxtPassword.setText("");
    }

    @Click
    protected void btnOk() {

        pd = ProgressDialog.show(this, "Login", "Conectando...", true, false);

        loginService.login(getUserLogin(), getPassLogin(), new SyncListener() {

            @Override
            public void onSyncStatusUpdated(String updateMessage) {
                pd.setMessage(updateMessage);
            }

            @Override
            public void onSyncFinished(Object result) {
                pd.dismiss();
                Toast.makeText(LoginActivity.this, "Conectado com sucesso!", Toast.LENGTH_LONG).show();
                gotoAvatarActivity();
            }

            @Override
            public void onSyncError(String txtMsg, Exception e) {
                pd.dismiss();
                Toast.makeText(LoginActivity.this, txtMsg, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void gotoAvatarActivity() {
        Intent i = new Intent(LoginActivity.this, AvatarActivity_.class);
        startActivity(i);
        finish();
    }

    private String getUserLogin(){
        return edTxtUsername.getText().toString();
    }

    private String getPassLogin(){
        return edTxtPassword.getText().toString();
    }
}