/**
 * 
 */
package br.com.teste.flowing.flowingteste.services.retrofit;

import java.util.List;

import br.com.teste.flowing.flowingteste.services.so.AvatarsSO;
import br.com.teste.flowing.flowingteste.services.so.LoginSO;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 *
 */
public interface IServices {
	
	/*LOGIN*/
	@FormUrlEncoded
	@POST("/account/gettoken")
	public void obterLogin(@Field("grant_type") String grantType,@Field("username") String login, @Field("password") String senha, Callback<LoginSO> callback);

	@GET("/v1/configurations/avatars")
	public void obterAvatares(@Header("Authorization") String completeToken, Callback<AvatarsSO[]> callback);

//	@GET("/v1/users/recomendedprograms")
//	public void obterAgendaProgramas(@Header("Authorization") String completeToken, Callback<AvatarsSO[]> callback);
}

