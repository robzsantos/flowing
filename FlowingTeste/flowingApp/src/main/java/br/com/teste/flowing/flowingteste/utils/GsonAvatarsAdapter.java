package br.com.teste.flowing.flowingteste.utils;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

import br.com.teste.flowing.flowingteste.services.so.AvatarsSO;

public class GsonAvatarsAdapter implements JsonDeserializer<AvatarsSO[]> {


	@Override
	public AvatarsSO[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {

		JsonArray jsonArray = json.getAsJsonArray();
		Gson gson = new Gson();
		AvatarsSO[] avatarsSOs = gson.fromJson(jsonArray.toString(), AvatarsSO[].class);

		return avatarsSOs;
	}
}