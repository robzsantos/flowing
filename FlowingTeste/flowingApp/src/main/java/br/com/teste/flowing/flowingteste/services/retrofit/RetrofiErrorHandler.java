package br.com.teste.flowing.flowingteste.services.retrofit;

import android.util.Log;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;

/**
 *
 */
public class RetrofiErrorHandler implements ErrorHandler {

	@Override 
	public Throwable handleError(RetrofitError cause) {
		/*Response r = cause.getResponse();
		if (r != null && r.getStatus() == 401) {
	      return new UnauthorizedException(cause);
	    }*/
		Log.e("retrofit", "Error message:" + cause.getMessage());
		return cause;
	}
}