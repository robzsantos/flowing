/**
 * 
 */
package br.com.teste.flowing.flowingteste.services;

import android.content.Context;
import android.util.Log;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.List;

import br.com.teste.flowing.flowingteste.services.retrofit.IServices;
import br.com.teste.flowing.flowingteste.services.retrofit.IServicesFactory;
import br.com.teste.flowing.flowingteste.services.so.AvatarsSO;
import br.com.teste.flowing.flowingteste.services.so.LoginSO;
import br.com.teste.flowing.flowingteste.utils.Preferencias_;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 *(scope=Scope.Singleton)
 */
@EBean
public class AvatarsService implements SyncListener {

	protected static final String TAG = AvatarsService.class.getName();

/*	@Bean
	protected InstrutorController instrutorController;

	@Bean
	protected MidiaController midiaController;

	@Bean
	protected SendData sendData;*/

	protected SyncListener listener;

	protected Context context;

	protected IServices iServices;

	@Pref
	protected Preferencias_ preferencias;

	public AvatarsService(Context context) {
		this.context = context;
		this.iServices = IServicesFactory.build(context);
	}

	public void getAvatars(final String tokenType, final String token, SyncListener listener){

		String completeToken = tokenType+" "+token;
		this.listener = listener;

		iServices.obterAvatares(completeToken, new Callback<AvatarsSO[]>() {
			@Override
			public void success(AvatarsSO[] avatarsSO, Response response) {
				Log.i(getTag(), avatarsSO.toString());
				onSyncFinished(avatarsSO);
			}

			@Override
			public void failure(RetrofitError error) {
				Log.i(getTag(), error.toString());
			}
		});
	}

/*	protected void onFinishAvatarsService(AvatarsSO[] result){

		AvatarsSO avatarsData = result;

		onSyncFinished(result);
	}*/

	@UiThread
	@Override
	public void onSyncStatusUpdated(String updateMessage) {
		Log.i(getTag(), updateMessage);

		if (listener != null){
			listener.onSyncStatusUpdated(updateMessage);
		}
	}

	@UiThread
	@Override
	public void onSyncFinished(Object result) {
		Log.i(getTag(), "Serviço finalizado!");

		if (listener != null){
			listener.onSyncFinished(result);
		}
	}

	@UiThread
	@Override
	public void onSyncError(String message, Exception e) {
		Log.e(getTag(), message, e);

/*		if (e != null){
			Mint.logException(getTag(), message, e);
		}*/

		if (listener != null){
			listener.onSyncError(message, e);
		}
	}

	public String getTag(){
		return this.getClass().getSimpleName();
	}

/*	protected String geraHash(String senha){
		return Encrypt.md5(senha);
	}*/
}