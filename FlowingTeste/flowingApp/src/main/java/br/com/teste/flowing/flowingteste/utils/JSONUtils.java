package br.com.teste.flowing.flowingteste.utils;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;
import java.util.List;

import br.com.teste.flowing.flowingteste.services.so.AvatarsSO;

/*import br.com.imonitore.core.enums.CategoriaCNHEnum;
import br.com.imonitore.core.enums.GravidadeInfracaoEnum;
*/

public class JSONUtils {

	private static GsonBuilder builder;
	
	static{
		builder = new GsonBuilder();
		builder.registerTypeAdapter(Date.class, new GsonDateAdapter());
		builder.registerTypeAdapter(AvatarsSO[].class, new GsonAvatarsAdapter());
/*
		builder.registerTypeAdapter(GravidadeInfracaoEnum.class, new GsonEnumAdapter());
*/
	}
	
	public static Gson newGson(){
		return builder.create();
	}
}
