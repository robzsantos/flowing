package br.com.teste.flowing.flowingteste.services;


/**
 * Listener para os eventos de sincronismo
 */
public interface SyncListener {
	
	public void onSyncStatusUpdated(String updateMessage);
	public void onSyncFinished(Object result);
	public void onSyncError(String message, Exception e);

}
