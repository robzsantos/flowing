package br.com.teste.flowing.flowingteste;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.WindowFeature;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;

import br.com.teste.flowing.flowingteste.services.AvatarsService;
import br.com.teste.flowing.flowingteste.services.SyncListener;
import br.com.teste.flowing.flowingteste.services.model.Avatar;
import br.com.teste.flowing.flowingteste.services.so.AvatarsSO;
import br.com.teste.flowing.flowingteste.utils.Preferencias_;

/**
 * Created by Robson on 05/06/2016.
 */
@WindowFeature({Window.FEATURE_NO_TITLE})
@EActivity(R.layout.activity_avatars)
public class AvatarActivity extends Activity {

    protected static final String TAG = AvatarActivity.class.getName();

    @ViewById
    protected RadioButton opt1;

    @ViewById
    protected RadioButton opt2;

    @ViewById
    protected RadioButton opt3;

    @ViewById
    protected ImageView picture1;

    @ViewById
    protected ImageView picture2;

    @ViewById
    protected ImageView picture3;

    @ViewById
    protected TextView nameAvatar1;

    @ViewById
    protected TextView nameAvatar2;

    @ViewById
    protected TextView nameAvatar3;

    @ViewById
    protected TextView avatarBio1;

    @ViewById
    protected TextView avatarBio2;

    @ViewById
    protected TextView avatarBio3;

    @Bean
    protected AvatarsService avatarService;

    private ProgressDialog pd;

    @Pref
    protected Preferencias_ preferencias;

    private ArrayList<Avatar> avatarModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @AfterInject
    protected void afterInject() {
        pd = ProgressDialog.show(this, "Login", "", true, false);
        avatarModels = new ArrayList<>();
        downloadAvatars();
    }

    @AfterViews
    protected void afterViews(){
        nameAvatar1.setText("");
        nameAvatar2.setText("");
        nameAvatar3.setText("");
        avatarBio1.setText("");
        avatarBio2.setText("");
        avatarBio3.setText("");
    }

    private void downloadAvatars(){
        pd.setMessage("Baixando lista de Avatares...");
        final Context context = getApplicationContext();

        avatarService.getAvatars(preferencias.tokenType().get(),preferencias.token().get(), new SyncListener() {

            @Override
            public void onSyncStatusUpdated(String updateMessage) {
                pd.setMessage(updateMessage);
            }

            @Override
            public void onSyncFinished(Object result) {
                pd.dismiss();
                Toast.makeText(AvatarActivity.this, "Lista de Avatares baixadas com sucesso!", Toast.LENGTH_LONG).show();
                for(AvatarsSO avatarSO:(AvatarsSO[]) result){
                    if(avatarSO.getAvatarBio() != null){
                        Picasso.with(context).load(avatarSO.getAvatarBio()).fetch();
                    }
                    Avatar avatar = new Avatar(avatarSO);
                    avatarModels.add(avatar);
                }
                fillScreenUI();
            }

            @Override
            public void onSyncError(String txtMsg, Exception e) {
                pd.dismiss();
                Toast.makeText(AvatarActivity.this, txtMsg, Toast.LENGTH_LONG).show();
            }
        });

    }

    private void fillScreenUI() {
        opt1.setVisibility(View.VISIBLE);
        nameAvatar1.setText(avatarModels.get(0).getName());
        avatarBio1.setText(avatarModels.get(0).getBiography());
        Picasso.with(this).load(avatarModels.get(0).getPicture()).into(picture1);

        opt2.setVisibility(View.VISIBLE);
        nameAvatar2.setText(avatarModels.get(1).getName());
        avatarBio2.setText(avatarModels.get(1).getBiography());
        Picasso.with(this).load(avatarModels.get(1).getPicture()).into(picture2);

        opt3.setVisibility(View.VISIBLE);
        nameAvatar3.setText(avatarModels.get(2).getName());
        avatarBio3.setText(avatarModels.get(2).getBiography());
        Picasso.with(this).load(avatarModels.get(2).getPicture()).into(picture3);
    }
}