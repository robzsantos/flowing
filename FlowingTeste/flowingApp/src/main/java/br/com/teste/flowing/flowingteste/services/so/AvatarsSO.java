package br.com.teste.flowing.flowingteste.services.so;


import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.BoolRes;

/**
 *
 */
public class AvatarsSO{

	private boolean IsSelected;
	private int AvatarId;
	private String AvatarPicturePath;
	private String AvatarBio;
	private String AvatarName;


	public boolean isSelected() {
		return IsSelected;
	}

	public void setSelected(boolean IsSelected) {
		this.IsSelected = IsSelected;
	}

	public int getAvatarId() {
		return this.AvatarId;
	}

	public void setAvatarId(int AvatarId) {
		this.AvatarId = AvatarId;
	}

	public String getAvatarPicturePath() {
		return this.AvatarPicturePath;
	}

	public void setAvatarPicturePath(String AvatarPicturePath) {
		this.AvatarPicturePath = AvatarPicturePath;
	}

	public String getAvatarBio() {
		return this.AvatarBio;
	}

	public void setAvatarBio(String AvatarBio) {
		this.AvatarBio = AvatarBio;
	}

	public String getAvatarName() {
		return this.AvatarName;
	}

	public void setAvatarName(String AvatarName) {
		this.AvatarName = AvatarName;
	}

}