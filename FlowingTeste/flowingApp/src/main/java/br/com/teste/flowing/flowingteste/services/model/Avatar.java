package br.com.teste.flowing.flowingteste.services.model;


import br.com.teste.flowing.flowingteste.services.so.AvatarsSO;

/**
 *
 */
public class Avatar {

	private boolean selected;
	private int id;
	private String picture;
	private String biography;
	private String name;

	public Avatar(AvatarsSO so){

		this.id = so.getAvatarId();
		this.selected = so.isSelected();
		this.picture = so.getAvatarPicturePath();
		this.biography = so.getAvatarBio();
		this.name = so.getAvatarName();

	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}