package br.com.teste.flowing.flowingteste.services.so;


/**
 *
 */
public class LoginSO {

	private String access_token;
	private String token_type;

	public String getToken() {
		return access_token;
	}

	public void setToken(String access_token) {
		this.access_token = access_token;
	}


	public String getTokenType() {
		return token_type;
	}

	public void setTokenType(String token_type) {
		this.token_type = token_type;
	}
}
