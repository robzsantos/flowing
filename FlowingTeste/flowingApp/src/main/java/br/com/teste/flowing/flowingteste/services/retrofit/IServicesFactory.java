/**
 * 
 */
package br.com.teste.flowing.flowingteste.services.retrofit;

import android.content.Context;
import android.util.Log;

import br.com.teste.flowing.flowingteste.R;
import br.com.teste.flowing.flowingteste.utils.JSONUtils;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 *
 */
public class IServicesFactory {
	
private static IServices service = null;
	
	public static IServices build(Context context){
		
		if (service == null){ 			
			service = create(context);
		}
		
		return service;
	}
	
	public static IServices create(Context context){
		
		RestAdapter ra = new RestAdapter.Builder()
		.setEndpoint(context.getString(R.string.baseUrl))
		.setConverter(new GsonConverter(JSONUtils.newGson()))
		.setErrorHandler(new RetrofiErrorHandler())
		.setLogLevel(RestAdapter.LogLevel.FULL).setLog(new RestAdapter.Log() {
            public void log(String msg) {
                Log.i("retrofit", msg);
             }
         })
		.build();
		
		return ra.create(IServices.class);
	}

}