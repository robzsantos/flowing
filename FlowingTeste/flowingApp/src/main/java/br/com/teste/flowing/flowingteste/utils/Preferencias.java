package br.com.teste.flowing.flowingteste.utils;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 *
 */
@SharedPref(value=SharedPref.Scope.UNIQUE)
public interface Preferencias {
	
/*
	String login();
	
	String hashSenha();
*/

	String token();

	String tokenType();
}